#include "ppu.hpp"

PictureProcessor::PictureProcessor()
{
    this->pal_screen_[0x00] = olc::Pixel(84, 84, 84);
    this->pal_screen_[0x01] = olc::Pixel(0, 30, 116);
    this->pal_screen_[0x02] = olc::Pixel(8, 16, 144);
    this->pal_screen_[0x03] = olc::Pixel(48, 0, 136);
    this->pal_screen_[0x04] = olc::Pixel(68, 0, 100);
    this->pal_screen_[0x05] = olc::Pixel(92, 0, 48);
    this->pal_screen_[0x06] = olc::Pixel(84, 4, 0);
    this->pal_screen_[0x07] = olc::Pixel(60, 24, 0);
    this->pal_screen_[0x08] = olc::Pixel(32, 42, 0);
    this->pal_screen_[0x09] = olc::Pixel(8, 58, 0);
    this->pal_screen_[0x0A] = olc::Pixel(0, 64, 0);
    this->pal_screen_[0x0B] = olc::Pixel(0, 60, 0);
    this->pal_screen_[0x0C] = olc::Pixel(0, 50, 60);
    this->pal_screen_[0x0D] = olc::Pixel(0, 0, 0);
    this->pal_screen_[0x0E] = olc::Pixel(0, 0, 0);
    this->pal_screen_[0x0F] = olc::Pixel(0, 0, 0);

    this->pal_screen_[0x10] = olc::Pixel(152, 150, 152);
    this->pal_screen_[0x11] = olc::Pixel(8, 76, 196);
    this->pal_screen_[0x12] = olc::Pixel(48, 50, 236);
    this->pal_screen_[0x13] = olc::Pixel(92, 30, 228);
    this->pal_screen_[0x14] = olc::Pixel(136, 20, 176);
    this->pal_screen_[0x15] = olc::Pixel(160, 20, 100);
    this->pal_screen_[0x16] = olc::Pixel(152, 34, 32);
    this->pal_screen_[0x17] = olc::Pixel(120, 60, 0);
    this->pal_screen_[0x18] = olc::Pixel(84, 90, 0);
    this->pal_screen_[0x19] = olc::Pixel(40, 114, 0);
    this->pal_screen_[0x1A] = olc::Pixel(8, 124, 0);
    this->pal_screen_[0x1B] = olc::Pixel(0, 118, 40);
    this->pal_screen_[0x1C] = olc::Pixel(0, 102, 120);
    this->pal_screen_[0x1D] = olc::Pixel(0, 0, 0);
    this->pal_screen_[0x1E] = olc::Pixel(0, 0, 0);
    this->pal_screen_[0x1F] = olc::Pixel(0, 0, 0);

    this->pal_screen_[0x20] = olc::Pixel(236, 238, 236);
    this->pal_screen_[0x21] = olc::Pixel(76, 154, 236);
    this->pal_screen_[0x22] = olc::Pixel(120, 124, 236);
    this->pal_screen_[0x23] = olc::Pixel(176, 98, 236);
    this->pal_screen_[0x24] = olc::Pixel(228, 84, 236);
    this->pal_screen_[0x25] = olc::Pixel(236, 88, 180);
    this->pal_screen_[0x26] = olc::Pixel(236, 106, 100);
    this->pal_screen_[0x27] = olc::Pixel(212, 136, 32);
    this->pal_screen_[0x28] = olc::Pixel(160, 170, 0);
    this->pal_screen_[0x29] = olc::Pixel(116, 196, 0);
    this->pal_screen_[0x2A] = olc::Pixel(76, 208, 32);
    this->pal_screen_[0x2B] = olc::Pixel(56, 204, 108);
    this->pal_screen_[0x2C] = olc::Pixel(56, 180, 204);
    this->pal_screen_[0x2D] = olc::Pixel(60, 60, 60);
    this->pal_screen_[0x2E] = olc::Pixel(0, 0, 0);
    this->pal_screen_[0x2F] = olc::Pixel(0, 0, 0);

    this->pal_screen_[0x30] = olc::Pixel(236, 238, 236);
    this->pal_screen_[0x31] = olc::Pixel(168, 204, 236);
    this->pal_screen_[0x32] = olc::Pixel(188, 188, 236);
    this->pal_screen_[0x33] = olc::Pixel(212, 178, 236);
    this->pal_screen_[0x34] = olc::Pixel(236, 174, 236);
    this->pal_screen_[0x35] = olc::Pixel(236, 174, 212);
    this->pal_screen_[0x36] = olc::Pixel(236, 180, 176);
    this->pal_screen_[0x37] = olc::Pixel(228, 196, 144);
    this->pal_screen_[0x38] = olc::Pixel(204, 210, 120);
    this->pal_screen_[0x39] = olc::Pixel(180, 222, 120);
    this->pal_screen_[0x3A] = olc::Pixel(168, 226, 144);
    this->pal_screen_[0x3B] = olc::Pixel(152, 226, 180);
    this->pal_screen_[0x3C] = olc::Pixel(160, 214, 228);
    this->pal_screen_[0x3D] = olc::Pixel(160, 162, 160);
    this->pal_screen_[0x3E] = olc::Pixel(0, 0, 0);
    this->pal_screen_[0x3F] = olc::Pixel(0, 0, 0);
}

bool PictureProcessor::can_handle(const uint16_t address, bool)
{
    return address >= 0x2000 && address <= 0x3FFF;
}

uint8_t PictureProcessor::read_byte(const uint16_t address)
{
    uint8_t data = 0;

    // ReSharper disable once CppDefaultCaseNotHandledInSwitchStatement
    switch (address)
    {
    case 0x2000:  // Control register
        break;

    case 0x2001:  // Mask register
        break;

    case 0x2002:  // Status register
        break;

    case 0x2003:  // OAM address register
        break;

    case 0x2004:  // OAM data register
        break;

    case 0x2005:  // Scroll register
        break;

    case 0x2006:  // Address register
        break;

    case 0x2007:  // Data register
        break;

    }

    return data;
}

uint16_t PictureProcessor::read_word(uint16_t)
{
    throw std::runtime_error("PPU does not support reading 16-bit values");
}

void PictureProcessor::write_byte(const uint16_t address, const uint8_t value)
{
    // ReSharper disable once CppDefaultCaseNotHandledInSwitchStatement
    switch (address)
    {
    case 0x2000:  // Control register
        break;

    case 0x2001:  // Mask register
        break;

    case 0x2002:  // Status register
        break;

    case 0x2003:  // OAM address register
        break;

    case 0x2004:  // OAM data register
        break;

    case 0x2005:  // Scroll register
        break;

    case 0x2006:  // Address register
        break;

    case 0x2007:  // Data register
        break;

    }
}

void PictureProcessor::write_word(uint16_t, uint16_t)
{
    throw std::runtime_error("PPU does not support writing 16-bit values");
}

void PictureProcessor::tick()
{
    this->screen_sprite_.SetPixel(this->cycle - 1, this->scan_line, this->pal_screen_[(rand() % 2) ? 0x3F : 0x30]);
    this->cycle++;

    if (this->cycle >= 341)
    {
        this->cycle = 0;
        this->scan_line++;

        if (this->scan_line >= 261)
        {
            this->scan_line = -1;
            this->frame_complete = true;
        }
    }
}

olc::Sprite& PictureProcessor::get_screen()
{
    return this->screen_sprite_;
}

olc::Sprite& PictureProcessor::get_name_table(const int index)
{
    return this->name_tables_sprites_[index];
}

olc::Sprite& PictureProcessor::get_pattern_table(const int index)
{
    return this->pattern_tables_sprites_[index];
}
