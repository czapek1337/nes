#pragma once

#include "../bus/bus.hpp"
#include "../olc_pge.hpp"

// ReSharper disable once CppPolymorphicClassWithNonVirtualPublicDestructor
class PictureProcessor : public BusDevice
{
    uint8_t name_tables_[2][1024] = { { 0 }, { 0 } };
    uint8_t pattern_tables_[2][4096] = { { 0 }, { 0 } };
    uint8_t palette_[32] = { 0 };

    olc::Pixel pal_screen_[64];

    olc::Sprite screen_sprite_ = olc::Sprite(256, 240);
    olc::Sprite name_tables_sprites_[2] = { olc::Sprite(256, 240), olc::Sprite(256, 240) };
    olc::Sprite pattern_tables_sprites_[2] = { olc::Sprite(128, 128), olc::Sprite(128, 128) };

public:
    bool frame_complete = false;
    int scan_line = 0;
    int cycle = 0;

    PictureProcessor();

    bool can_handle(uint16_t address, bool write = false) override;

    uint8_t read_byte(uint16_t address) override;
    uint16_t read_word(uint16_t address) override;

    void write_byte(uint16_t address, uint8_t value) override;
    void write_word(uint16_t address, uint16_t value) override;

    void tick();

    olc::Sprite& get_screen();
    olc::Sprite& get_name_table(int index);
    olc::Sprite& get_pattern_table(int index);

};
