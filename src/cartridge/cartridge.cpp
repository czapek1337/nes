#include <fstream>

#include "cartridge.hpp"

void NesHeader::initialize(const std::vector<uint8_t>& data)
{
    this->prg_rom_size = data[4];
    this->chr_rom_size = data[5];
    this->flags6 = data[6];
    this->flags7 = data[7];
    this->prg_ram_size = data[8];
    this->flags9 = data[9];
    this->flags10 = data[10];
}

uint8_t NesHeader::mapper() const
{
    return (this->flags7 & 0xF0) | this->flags6 >> 4;
}

uint8_t NesHeader::ines_mapper() const
{
    return this->flags6 >> 4;
}

bool NesHeader::trainer() const
{
    return (this->flags6 & 0x04) != 0;
}

Cartridge::Cartridge(const std::string& filename)
{
    std::ifstream input_file(filename.data(), std::ios::binary);
    std::vector<uint8_t> header_data;

    header_data.resize(16);
    input_file.read(reinterpret_cast<char*>(header_data.data()), 16);

    this->header_.initialize(header_data);
    this->program_data_.resize(this->header_.prg_rom_size * (1024 * 16));
    this->character_data_.resize(this->header_.chr_rom_size * (1024 * 8));

    input_file.read(reinterpret_cast<char*>(this->program_data_.data()), this->header_.prg_rom_size * (1024 * 16));
    input_file.read(reinterpret_cast<char*>(this->character_data_.data()), this->header_.chr_rom_size * (1024 * 8));
}

bool Cartridge::can_handle(const uint16_t address, const bool)
{
    return address >= 0x4020 && address <= 0xFFFF;
}

uint8_t Cartridge::read_byte(const uint16_t address)
{
    if (address < 0x8000)
        return 0;

    return this->program_data_[address & 0x3FFF];
}

uint16_t Cartridge::read_word(const uint16_t address)
{
    if (address < 0x8000)
        return 0;

    return this->program_data_[address & 0x3FFF] | this->program_data_[address + 1 & 0x3FFF] << 8;
}

void Cartridge::write_byte(const uint16_t address, uint8_t value)
{
    if (address < 0x8000)
        return;

    throw std::runtime_error("Cannot write into read-only memory");
}

void Cartridge::write_word(const uint16_t address, uint16_t value)
{
    if (address < 0x8000)
        return;

    throw std::runtime_error("Cannot write into read-only memory");
}
