#pragma once

#include <cstdint>
#include <vector>

#include "../bus/bus.hpp"

struct NesHeader
{
    uint8_t prg_rom_size;
    uint8_t chr_rom_size;
    uint8_t flags6;
    uint8_t flags7;
    uint8_t prg_ram_size;
    uint8_t flags9;
    uint8_t flags10;

    void initialize(const std::vector<uint8_t>& data);

    uint8_t mapper() const;
    uint8_t ines_mapper() const;

    bool trainer() const;

};

// ReSharper disable once CppPolymorphicClassWithNonVirtualPublicDestructor
class Cartridge : public BusDevice
{
    std::vector<uint8_t> program_data_;
    std::vector<uint8_t> character_data_;

    NesHeader header_;

public:
    explicit Cartridge(const std::string& filename);

    bool can_handle(uint16_t address, bool write = false) override;

    uint8_t read_byte(uint16_t address) override;
    uint16_t read_word(uint16_t address) override;

    void write_byte(uint16_t address, uint8_t value) override;
    void write_word(uint16_t address, uint16_t value) override;

};
