#include "cpu.hpp"

#define NOT_IMPLEMENTED(op)                                     \
    void Processor::##op(const AddressingMode addressing_mode)  \
    {                                                           \
        return this->xxx(addressing_mode);                      \
    }                                                           \

#define OPCODE_LOAD(mnemonic, register_)                                                            \
    void Processor::##mnemonic(const AddressingMode addressing_mode)                                \
    {                                                                                               \
        this->##register_ = this->bus_->read_byte(this->resolve_addressing_mode(addressing_mode));  \
                                                                                                    \
        this->set_flag(ProcessorFlag::ZERO, this->##register_ == 0);                                \
        this->set_flag(ProcessorFlag::NEGATIVE, this->##register_ & 0x80);                          \
                                                                                                    \
        this->xxx(addressing_mode);                                                                 \
    }                                                                                               \

#define OPCODE_STORE(mnemonic, register_)                                                           \
    void Processor::##mnemonic(const AddressingMode addressing_mode)                                \
    {                                                                                               \
        this->bus_->write_byte(this->resolve_addressing_mode(addressing_mode), this->##register_);  \
        this->xxx(addressing_mode);                                                                 \
    }                                                                                               \

#define OPCODE_TRANSFER(mnemonic, source, target, update_flags)             \
    void Processor::##mnemonic(const AddressingMode addressing_mode)        \
    {                                                                       \
        this->##target = this->##source;                                    \
                                                                            \
        if (update_flags)                                                   \
        {                                                                   \
            this->set_flag(ProcessorFlag::ZERO, this->##target == 0);       \
            this->set_flag(ProcessorFlag::NEGATIVE, this->##target & 0x80); \
        }                                                                   \
                                                                            \
        this->xxx(addressing_mode);                                         \
    }                                                                       \

#define OPCODE_BRANCH(mnemonic, flag, value)                                            \
    void Processor::##mnemonic(const AddressingMode addressing_mode)                    \
    {                                                                                   \
        if (this->get_flag(flag) == value)                                              \
            this->program_counter += this->resolve_addressing_mode(addressing_mode);   \
        else                                                                            \
            this->xxx(addressing_mode);                                                 \
    }                                                                                   \

#define OPCODE_FLAG(mnemonic, flag, value)                              \
    void Processor::##mnemonic(const AddressingMode addressing_mode)    \
    {                                                                   \
        this->set_flag(flag, value);                                    \
        this->xxx(addressing_mode);                                     \
    }                                                                   \

#define OPCODE_COMPARE(mnemonic, register_)                                                         \
    void Processor::##mnemonic(const AddressingMode addressing_mode)                                \
    {                                                                                               \
        const auto value = this->resolve_addressing_mode(addressing_mode);                          \
        const auto temp = static_cast<uint16_t>(this->##register_) - static_cast<uint16_t>(value);  \
                                                                                                    \
        this->set_flag(ProcessorFlag::CARRY, this->##register_ >= value);                           \
        this->set_flag(ProcessorFlag::ZERO, (temp & 0x00FF) == 0);                                  \
        this->set_flag(ProcessorFlag::NEGATIVE, temp & 0x0080);                                     \
                                                                                                    \
        this->xxx(addressing_mode);                                                                 \
    }                                                                                               \

uint8_t ProcessorFlags::get_uint() const
{
#define FLAG_HANDLER(index) if (this->value[index]) { result |= 1 << index; }

    uint8_t result = 0;

    FLAG_HANDLER(0); // CARRY
    FLAG_HANDLER(1); // ZERO
    FLAG_HANDLER(2); // INTERRUPT_DISABLE
    FLAG_HANDLER(3); // DECIMAL_MODE
    FLAG_HANDLER(4); // BREAK
    FLAG_HANDLER(5); // UNUSED
    FLAG_HANDLER(6); // OVERFLOW
    FLAG_HANDLER(7); // NEGATIVE

    return result;

#undef FLAG_HANDLER
}

void ProcessorFlags::set_uint(const uint8_t uint)
{
#define FLAG_HANDLER(index) this->value[index] = uint & (1 << index);

    FLAG_HANDLER(0); // CARRY
    FLAG_HANDLER(1); // ZERO
    FLAG_HANDLER(2); // INTERRUPT_DISABLE
    FLAG_HANDLER(3); // DECIMAL_MODE
    FLAG_HANDLER(4); // BREAK
    FLAG_HANDLER(5); // UNUSED
    FLAG_HANDLER(6); // OVERFLOW
    FLAG_HANDLER(7); // NEGATIVE

#undef FLAG_HANDLER
}

int Processor::get_bytes_needed(const AddressingMode addressing_mode)
{
    switch (addressing_mode)
    {
        case AddressingMode::ABSOLUTE: return 2;
        case AddressingMode::ABSOLUTE_X: return 2;
        case AddressingMode::ABSOLUTE_Y: return 2;
        case AddressingMode::IMMEDIATE: return 1;
        case AddressingMode::IMPLIED: return 0;
        case AddressingMode::RELATIVE: return 1;
        case AddressingMode::INDIRECT: return 2;
        case AddressingMode::INDEXED_INDIRECT: return 2;
        case AddressingMode::INDIRECT_INDEXED: return 2;
        case AddressingMode::ZERO_PAGE: return 1;
        case AddressingMode::ZERO_PAGE_X: return 1;
        case AddressingMode::ZERO_PAGE_Y: return 1;
    }

    throw std::runtime_error("Unknown addressing mode");
}

void Processor::xxx(const AddressingMode addressing_mode)
{
    this->program_counter += get_bytes_needed(addressing_mode);
}

void Processor::jmp(const AddressingMode addressing_mode)
{
    auto jmp_target = this->bus_->read_word(this->program_counter);

    if (addressing_mode == AddressingMode::INDIRECT)
        jmp_target = this->bus_->read_word(jmp_target);

    this->program_counter = jmp_target;
}

void Processor::jsr(const AddressingMode addressing_mode)
{
    this->push_word(this->program_counter);

    this->program_counter = this->bus_->read_word(this->program_counter);
}

void Processor::bit(const AddressingMode addressing_mode)
{
    const auto value = this->resolve_addressing_mode(addressing_mode);
    const auto temp = this->accumulator & value;

    this->set_flag(ProcessorFlag::ZERO, (temp & 0x00FF) == 0x00);
    this->set_flag(ProcessorFlag::NEGATIVE, value & 1 << 7);
    this->set_flag(ProcessorFlag::OVERFLOW, value & 1 << 6);
}

void Processor::plp(const AddressingMode addressing_mode)
{
    this->flags.set_uint(this->pop_byte());

    this->xxx(addressing_mode);
}

void Processor::pla(const AddressingMode addressing_mode)
{
    this->accumulator = this->pop_byte();

    this->set_flag(ProcessorFlag::ZERO, this->accumulator == 0);
    this->set_flag(ProcessorFlag::NEGATIVE, this->accumulator & 0x80);

    this->xxx(addressing_mode);
}

void Processor::php(const AddressingMode addressing_mode)
{
    this->push_byte(this->flags.get_uint() | 1 << 4);
    this->xxx(addressing_mode);
}

void Processor::pha(const AddressingMode addressing_mode)
{
    this->push_byte(this->accumulator);
    this->xxx(addressing_mode);
}

void Processor::rti(const AddressingMode addressing_mode)
{
    this->flags.set_uint(this->pop_byte());
    this->program_counter = this->pop_word() + 1;
}

void Processor::rts(const AddressingMode addressing_mode)
{
    this->program_counter = this->pop_word() + 1;
}

OPCODE_LOAD(lda, accumulator);
OPCODE_LOAD(ldx, index_x);
OPCODE_LOAD(ldy, index_y);

OPCODE_STORE(sta, accumulator);
OPCODE_STORE(stx, index_x);
OPCODE_STORE(sty, index_y);

OPCODE_TRANSFER(tax, accumulator, index_x, true);
OPCODE_TRANSFER(tay, accumulator, index_y, true);
OPCODE_TRANSFER(tsx, stack_pointer, index_x, true);
OPCODE_TRANSFER(txa, index_x, accumulator, true);
OPCODE_TRANSFER(txs, index_x, stack_pointer, false);
OPCODE_TRANSFER(tya, index_y, accumulator, true);

OPCODE_BRANCH(bcc, ProcessorFlag::CARRY, false);
OPCODE_BRANCH(bcs, ProcessorFlag::CARRY, true);
OPCODE_BRANCH(beq, ProcessorFlag::ZERO, true);
OPCODE_BRANCH(bmi, ProcessorFlag::NEGATIVE, true);
OPCODE_BRANCH(bne, ProcessorFlag::ZERO, false);
OPCODE_BRANCH(bpl, ProcessorFlag::NEGATIVE, false);
OPCODE_BRANCH(bvc, ProcessorFlag::OVERFLOW, false);
OPCODE_BRANCH(bvs, ProcessorFlag::OVERFLOW, true);

OPCODE_FLAG(clc, ProcessorFlag::CARRY, false);
OPCODE_FLAG(cld, ProcessorFlag::DECIMAL_MODE, false);
OPCODE_FLAG(cli, ProcessorFlag::INTERRUPT_DISABLE, false);
OPCODE_FLAG(clv, ProcessorFlag::OVERFLOW, false);

OPCODE_FLAG(sec, ProcessorFlag::CARRY, true);
OPCODE_FLAG(sed, ProcessorFlag::DECIMAL_MODE, true);
OPCODE_FLAG(sei, ProcessorFlag::INTERRUPT_DISABLE, true);

OPCODE_COMPARE(cmp, accumulator);
OPCODE_COMPARE(cpx, index_x);
OPCODE_COMPARE(cpy, index_y);

NOT_IMPLEMENTED(adc);
NOT_IMPLEMENTED(and_);
NOT_IMPLEMENTED(asl);
NOT_IMPLEMENTED(brk);
NOT_IMPLEMENTED(dec);
NOT_IMPLEMENTED(dex);
NOT_IMPLEMENTED(dey);
NOT_IMPLEMENTED(eor);
NOT_IMPLEMENTED(inc);
NOT_IMPLEMENTED(inx);
NOT_IMPLEMENTED(iny);
NOT_IMPLEMENTED(lsr);
NOT_IMPLEMENTED(nop);
NOT_IMPLEMENTED(ora);
NOT_IMPLEMENTED(rol);
NOT_IMPLEMENTED(ror);
NOT_IMPLEMENTED(sbc);
