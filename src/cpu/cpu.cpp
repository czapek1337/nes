#include "cpu.hpp"

int Processor::get_cycle_count(const uint8_t opcode)
{
    // ReSharper disable once CppDefaultCaseNotHandledInSwitchStatement
    switch (opcode)
    {
    case 0x02: case 0x12: case 0x22: case 0x32: case 0x42: case 0x52: case 0x62: case 0x72: case 0x92: case 0xB2: case 0xD2: case 0xF2:
        return 0;

    case 0x09: case 0x0A: case 0x0B: case 0x10: case 0x18: case 0x1A: case 0x29: case 0x2A: case 0x2B: case 0x30: case 0x38: case 0x3A:
    case 0x49: case 0x4A: case 0x4B: case 0x50: case 0x58: case 0x5A: case 0x69: case 0x6A: case 0x6B: case 0x70: case 0x78: case 0x7A:
    case 0x80: case 0x82: case 0x88: case 0x89: case 0x8A: case 0x8B: case 0x90: case 0x98: case 0x9A: case 0xA0: case 0xA2: case 0xA8:
    case 0xA9: case 0xAA: case 0xAB: case 0xB0: case 0xB8: case 0xBA: case 0xC0: case 0xC2: case 0xC8: case 0xC9: case 0xCA: case 0xCB:
    case 0xD0: case 0xD8: case 0xDA: case 0xE0: case 0xE2: case 0xE8: case 0xE9: case 0xEA: case 0xEB: case 0xF0: case 0xF8: case 0xFA:
        return 2;

    case 0x04: case 0x05: case 0x08: case 0x24: case 0x25: case 0x44: case 0x45: case 0x48: case 0x4C: case 0x64: case 0x65: case 0x84:
    case 0x85: case 0x86: case 0x87: case 0xA4: case 0xA5: case 0xA6: case 0xA7: case 0xC4: case 0xC5: case 0xE4: case 0xE5:
        return 3;

    case 0x0C: case 0x0D: case 0x14: case 0x15: case 0x19: case 0x1C: case 0x1D: case 0x28: case 0x2C: case 0x2D: case 0x34: case 0x35:
    case 0x39: case 0x3C: case 0x3D: case 0x4D: case 0x54: case 0x55: case 0x59: case 0x5C: case 0x5D: case 0x68: case 0x6D: case 0x74:
    case 0x75: case 0x79: case 0x7C: case 0x7D: case 0x8C: case 0x8D: case 0x8E: case 0x8F: case 0x94: case 0x95: case 0x96: case 0x97:
    case 0xAC: case 0xAD: case 0xAE: case 0xAF: case 0xB4: case 0xB5: case 0xB6: case 0xB7: case 0xB9: case 0xBB: case 0xBC: case 0xBD:
    case 0xBE: case 0xBF: case 0xCC: case 0xCD: case 0xD4: case 0xD5: case 0xD9: case 0xDC: case 0xDD: case 0xEC: case 0xED: case 0xF4:
    case 0xF5: case 0xF9: case 0xFC: case 0xFD:
        return 4;

    case 0x06: case 0x07: case 0x11: case 0x26: case 0x27: case 0x31: case 0x46: case 0x47: case 0x51: case 0x66: case 0x67: case 0x6C:
    case 0x71: case 0x99: case 0x9B: case 0x9C: case 0x9D: case 0x9E: case 0x9F: case 0xB1: case 0xB3: case 0xC6: case 0xC7: case 0xD1:
    case 0xE6: case 0xE7: case 0xF1:
        return 5;

    case 0x01: case 0x0E: case 0x0F: case 0x16: case 0x17: case 0x20: case 0x21: case 0x2E: case 0x2F: case 0x36: case 0x37: case 0x40:
    case 0x41: case 0x4E: case 0x4F: case 0x56: case 0x57: case 0x60: case 0x61: case 0x6E: case 0x6F: case 0x76: case 0x77: case 0x81:
    case 0x83: case 0x91: case 0x93: case 0xA1: case 0xA3: case 0xC1: case 0xCE: case 0xCF: case 0xD6: case 0xD7: case 0xE1: case 0xEE:
    case 0xEF: case 0xF6: case 0xF7:
        return 6;

    case 0x00: case 0x1B: case 0x1E: case 0x1F: case 0x3B: case 0x3E: case 0x3F: case 0x5B: case 0x5E: case 0x5F: case 0x7B: case 0x7E:
    case 0x7F: case 0xDB: case 0xDE: case 0xDF: case 0xFB: case 0xFE: case 0xFF:
        return 7;

    case 0x03: case 0x13: case 0x23: case 0x33: case 0x43: case 0x53: case 0x63: case 0x73: case 0xC3: case 0xD3: case 0xE3: case 0xF3:
        return 8;

    }

    throw std::runtime_error("Unknown instruction");
}

uint16_t Processor::resolve_addressing_mode(const AddressingMode addressing_mode)
{
    switch (addressing_mode)
    {
    case AddressingMode::ABSOLUTE: return this->bus_->read_word(this->program_counter);
    case AddressingMode::ABSOLUTE_X: return this->bus_->read_word(this->program_counter) + this->index_x;
    case AddressingMode::ABSOLUTE_Y: return this->bus_->read_word(this->program_counter) + this->index_y;
    case AddressingMode::IMMEDIATE: return this->program_counter;
    case AddressingMode::IMPLIED: return 0;
    case AddressingMode::RELATIVE: { const auto byte = this->bus_->read_byte(this->program_counter) + 1; return byte & 0x80 ? byte | 0xFF00 : byte; }
    case AddressingMode::INDIRECT: return this->bus_->read_word(this->bus_->read_word(this->program_counter));
    case AddressingMode::INDEXED_INDIRECT: throw std::runtime_error("Indexed-indirect addressing mode is not implemented yet");
    case AddressingMode::INDIRECT_INDEXED: throw std::runtime_error("Indirect-indexed addressing mode is not implemented yet");
    case AddressingMode::ZERO_PAGE: return this->bus_->read_byte(this->program_counter);
    case AddressingMode::ZERO_PAGE_X: return this->bus_->read_byte(this->program_counter) + this->index_x;
    case AddressingMode::ZERO_PAGE_Y: return this->bus_->read_byte(this->program_counter) + this->index_y;
    }

    throw std::runtime_error("Unknown addressing mode");
}

AddressingMode Processor::get_addressing_mode(const uint8_t opcode)
{
    // ReSharper disable once CppDefaultCaseNotHandledInSwitchStatement
    switch (opcode)
    {
    case 0x00: case 0x08: case 0x0A: case 0x18: case 0x28:
    case 0x2A: case 0x38: case 0x40: case 0x48: case 0x4A:
    case 0x58: case 0x60: case 0x68: case 0x6A: case 0x78:
    case 0x88: case 0x8A: case 0x98: case 0x9A: case 0xA8:
    case 0xAA: case 0xB8: case 0xBA: case 0xC8: case 0xCA:
    case 0xD8: case 0xE8: case 0xEA: case 0xF8:
        return AddressingMode::IMPLIED;

    case 0x01: case 0x21: case 0x41: case 0x61: case 0x81:
    case 0xA1: case 0xC1: case 0xE1:
        return AddressingMode::INDEXED_INDIRECT;

    case 0x05: case 0x06: case 0x24: case 0x25: case 0x26:
    case 0x45: case 0x46: case 0x65: case 0x66: case 0x84:
    case 0x85: case 0x86: case 0xA4: case 0xA5: case 0xA6:
    case 0xC4: case 0xC5: case 0xC6: case 0xE4: case 0xE5:
    case 0xE6:
        return AddressingMode::ZERO_PAGE;

    case 0x09: case 0x29: case 0x49: case 0x69: case 0xA0:
    case 0xA2: case 0xA9: case 0xC0: case 0xC9: case 0xE0:
    case 0xE9:
        return AddressingMode::IMMEDIATE;

    case 0x0D: case 0x0E: case 0x20: case 0x2C: case 0x2D:
    case 0x2E: case 0x4C: case 0x4D: case 0x4E: case 0x6D:
    case 0x6E: case 0x8C: case 0x8D: case 0x8E: case 0xAC:
    case 0xAD: case 0xAE: case 0xCC: case 0xCD: case 0xCE:
    case 0xEC: case 0xED: case 0xEE:
        return AddressingMode::ABSOLUTE;

    case 0x10: case 0x30: case 0x50: case 0x70: case 0x90:
    case 0xB0: case 0xD0: case 0xF0:
        return AddressingMode::RELATIVE;

    case 0x11: case 0x31: case 0x51: case 0x71: case 0x91:
    case 0xB1: case 0xD1: case 0xF1:
        return AddressingMode::INDIRECT_INDEXED;

    case 0x15: case 0x16: case 0x35: case 0x36: case 0x55:
    case 0x56: case 0x75: case 0x76: case 0x94: case 0x95:
    case 0xB4: case 0xB5: case 0xD5: case 0xD6: case 0xF5:
    case 0xF6:
        return AddressingMode::ZERO_PAGE_X;

    case 0x19: case 0x39: case 0x59: case 0x79: case 0x99:
    case 0xB9: case 0xBE: case 0xD9: case 0xF9:
        return AddressingMode::ABSOLUTE_Y;

    case 0x1D: case 0x1E: case 0x3D: case 0x3E: case 0x5D:
    case 0x5E: case 0x7D: case 0x7E: case 0x9D: case 0xBC:
    case 0xBD: case 0xDD: case 0xDE: case 0xFD: case 0xFE:
        return AddressingMode::ABSOLUTE_X;

    case 0x6C:
        return AddressingMode::INDIRECT;

    case 0x96: case 0xB6:
        return AddressingMode::ZERO_PAGE_Y;

    }

    throw std::runtime_error("Unknown addressing mode");
}

std::string Processor::get_opcode_mnemonic(const uint8_t opcode)
{
    // ReSharper disable once CppDefaultCaseNotHandledInSwitchStatement
    switch (opcode)
    {
    case 0x00: return "BRK";
    case 0x08: return "PHP";
    case 0x10: return "BPL";
    case 0x18: return "CLC";
    case 0x20: return "JSR";
    case 0x28: return "PLP";
    case 0x30: return "BMI";
    case 0x38: return "SEC";
    case 0x40: return "RTI";
    case 0x48: return "PHA";
    case 0x50: return "BVC";
    case 0x58: return "CLI";
    case 0x60: return "RTS";
    case 0x68: return "PLA";
    case 0x70: return "BVS";
    case 0x78: return "SEI";
    case 0x88: return "DEY";
    case 0x8A: return "TXA";
    case 0x90: return "BCC";
    case 0x98: return "TYA";
    case 0x9A: return "TXS";
    case 0xA8: return "TAY";
    case 0xAA: return "TAX";
    case 0xB0: return "BCS";
    case 0xB8: return "CLV";
    case 0xBA: return "TSX";
    case 0xC8: return "INY";
    case 0xCA: return "DEX";
    case 0xD0: return "BNE";
    case 0xD8: return "CLD";
    case 0xE8: return "INX";
    case 0xEA: return "NOP";
    case 0xF0: return "BEQ";
    case 0xF8: return "SED";

    case 0x01: case 0x05: case 0x09: case 0x0D: case 0x11:
    case 0x15: case 0x19: case 0x1D:
        return "ORA";

    case 0x06: case 0x0A: case 0x0E: case 0x16: case 0x1E:
        return "ASL";

    case 0x21: case 0x25: case 0x29: case 0x2D: case 0x31:
    case 0x35: case 0x39: case 0x3D:
        return "AND";

    case 0x24: case 0x2C:
        return "BIT";

    case 0x26: case 0x2A: case 0x2E: case 0x36: case 0x3E:
        return "ROL";

    case 0x41: case 0x45: case 0x49: case 0x4D: case 0x51:
    case 0x55: case 0x59: case 0x5D:
        return "EOR";

    case 0x46: case 0x4A: case 0x4E: case 0x56: case 0x5E:
        return "LSR";

    case 0x4C: case 0x6C:
        return "JMP";

    case 0x61: case 0x65: case 0x69: case 0x6D: case 0x71:
    case 0x75: case 0x79: case 0x7D:
        return "ADC";

    case 0x66: case 0x6A: case 0x6E: case 0x76: case 0x7E:
        return "ROR";

    case 0x81: case 0x85: case 0x8D: case 0x91: case 0x95:
    case 0x99: case 0x9D:
        return "STA";

    case 0x84: case 0x8C: case 0x94:
        return "STY";

    case 0x86: case 0x8E: case 0x96:
        return "STX";

    case 0xA0: case 0xA4: case 0xAC: case 0xB4: case 0xBC:
        return "LDY";

    case 0xA1: case 0xA5: case 0xA9: case 0xAD: case 0xB1:
    case 0xB5: case 0xB9: case 0xBD:
        return "LDA";

    case 0xA2: case 0xA6: case 0xAE: case 0xB6: case 0xBE:
        return "LDX";

    case 0xC0: case 0xC4: case 0xCC:
        return "CPY";

    case 0xC1: case 0xC5: case 0xC9: case 0xCD: case 0xD1:
    case 0xD5: case 0xD9: case 0xDD:
        return "CMP";

    case 0xC6: case 0xCE: case 0xD6: case 0xDE:
        return "DEC";

    case 0xE0: case 0xE4: case 0xEC:
        return "CPX";

    case 0xE1: case 0xE5: case 0xE9: case 0xED: case 0xF1:
    case 0xF5: case 0xF9: case 0xFD:
        return "SBC";

    case 0xE6: case 0xEE: case 0xF6: case 0xFE:
        return "INC";

    }

    throw std::runtime_error("Unknown opcode");
}

std::string Processor::make_flagsstring(const uint8_t flags)
{
#define FLAG_HANDLER(index, char_) if (flags & (1 << index)) { result.at(7 - index) = char_; }

    std::string result(8, '-');

    FLAG_HANDLER(0, 'C');
    FLAG_HANDLER(1, 'Z');
    FLAG_HANDLER(2, 'I');
    FLAG_HANDLER(3, 'D');
    FLAG_HANDLER(4, 'B');
    FLAG_HANDLER(5, 'U');
    FLAG_HANDLER(6, 'V');
    FLAG_HANDLER(7, 'N');

    return result;

#undef FLAG_HANDLER
}

std::string Processor::get_addressing_mode_str(const AddressingMode addressing_mode)
{
    switch (addressing_mode)
    {
    case AddressingMode::ABSOLUTE: return "ABS";
    case AddressingMode::ABSOLUTE_X: return "ABS,X";
    case AddressingMode::ABSOLUTE_Y: return "ABS,Y";
    case AddressingMode::IMMEDIATE: return "IMM";
    case AddressingMode::IMPLIED: return "IMP";
    case AddressingMode::RELATIVE: return "REL";
    case AddressingMode::INDIRECT: return "IND";
    case AddressingMode::INDEXED_INDIRECT: return "X,IND";
    case AddressingMode::INDIRECT_INDEXED: return "IND,Y";
    case AddressingMode::ZERO_PAGE: return "ZPG";
    case AddressingMode::ZERO_PAGE_X: return "ZPG,X";
    case AddressingMode::ZERO_PAGE_Y: return "ZPG,Y";
    }

    throw std::runtime_error("Unknown addressing mode");
}

bool Processor::get_flag(const ProcessorFlag flag)
{
#define FLAG_HANDLER(index) return this->flags.value[index];

    switch (flag)
    {
    case ProcessorFlag::CARRY: FLAG_HANDLER(0);
    case ProcessorFlag::ZERO: FLAG_HANDLER(1);
    case ProcessorFlag::INTERRUPT_DISABLE: FLAG_HANDLER(2);
    case ProcessorFlag::DECIMAL_MODE: FLAG_HANDLER(3);
    case ProcessorFlag::BREAK: FLAG_HANDLER(4);
    case ProcessorFlag::UNUSED: FLAG_HANDLER(5);
    case ProcessorFlag::OVERFLOW: FLAG_HANDLER(6);
    case ProcessorFlag::NEGATIVE: FLAG_HANDLER(7);
    }

    throw std::runtime_error("Unknown processor flag");

#undef FLAG_HANDLER
}

void Processor::set_flag(const ProcessorFlag flag, const bool value)
{
#define FLAG_HANDLER(index) this->flags.value[index] = value;

    switch (flag)
    {
    case ProcessorFlag::CARRY: FLAG_HANDLER(0); return;
    case ProcessorFlag::ZERO: FLAG_HANDLER(1); return;
    case ProcessorFlag::INTERRUPT_DISABLE: FLAG_HANDLER(2); return;
    case ProcessorFlag::DECIMAL_MODE: FLAG_HANDLER(3); return;
    case ProcessorFlag::BREAK: FLAG_HANDLER(4); return;
    case ProcessorFlag::UNUSED: FLAG_HANDLER(5); return;
    case ProcessorFlag::OVERFLOW: FLAG_HANDLER(6); return;
    case ProcessorFlag::NEGATIVE: FLAG_HANDLER(7); return;
    }

    throw std::runtime_error("Unknown processor flag");

#undef FLAG_HANDLER
}

void Processor::push_byte(const uint8_t value)
{
    this->bus_->write_byte(0x0100 + (this->stack_pointer--), value);
}

void Processor::push_word(const uint16_t value)
{
    this->push_byte(value >> 8);
    this->push_byte(value & 0xFF);
}

uint8_t Processor::pop_byte()
{
    return this->bus_->read_byte(0x0100 + ++this->stack_pointer);
}

uint16_t Processor::pop_word()
{
    return this->pop_byte() | this->pop_byte() << 8;
}

Processor::Processor(const std::shared_ptr<DataBus>& bus, const uint16_t program_counter)
    : bus_(bus)
    , accumulator(0)
    , index_x(0)
    , index_y(0)
    , stack_pointer(0xFD)
    , program_counter(program_counter)
    , cycles(0)
{
    this->power_up();
}

void Processor::power_up()
{
    this->accumulator = 0;
    this->index_x = 0;
    this->index_y = 0;
    this->stack_pointer = 0xFD;
    this->flags.set_uint(0x20);
    this->cycles = 7;

    if (this->program_counter == 0)
        this->program_counter = this->bus_->read_word(0xFFFC);
}

void Processor::reset()
{
    this->stack_pointer -= 0x03;
    this->cycles += 7;
    this->program_counter = this->bus_->read_word(0xFFFC);
}

void Processor::trace() const
{
    const auto opcode = this->bus_->read_byte(this->program_counter);

    const auto addressing_mode = get_addressing_mode(opcode);
    const auto mnemonic = get_opcode_mnemonic(opcode);

    const auto byte_1 = this->bus_->read_byte(this->program_counter + 1);
    const auto byte_2 = this->bus_->read_byte(this->program_counter + 2);

    switch (get_bytes_needed(addressing_mode))
    {
    case 0: printf("$%04X    %s                ", this->program_counter, mnemonic.data()); break;
    case 1: printf("$%04X    %s %02X             ", this->program_counter, mnemonic.data(), byte_1); break;
    case 2: printf("$%04X    %s %02X %02X          ", this->program_counter, mnemonic.data(), byte_1, byte_2); break;

    }

    printf("A: $%02X  X: $%02X  Y: $%02X  SP: $%02X  FLAGS: %s  CYCLES: %lld \n",
        this->accumulator, this->index_x, this->index_y, this->stack_pointer, make_flagsstring(this->flags.get_uint()).data(), this->cycles);
}

void Processor::tick()
{
    const auto opcode = this->bus_->read_byte(this->program_counter++);
    const auto addressing_mode = get_addressing_mode(opcode);

    // ReSharper disable once CppDefaultCaseNotHandledInSwitchStatement
    switch (opcode)
    {
    case 0x00: this->brk(addressing_mode); break;
    case 0x08: this->php(addressing_mode); break;
    case 0x10: this->bpl(addressing_mode); break;
    case 0x18: this->clc(addressing_mode); break;
    case 0x20: this->jsr(addressing_mode); break;
    case 0x28: this->plp(addressing_mode); break;
    case 0x30: this->bmi(addressing_mode); break;
    case 0x38: this->sec(addressing_mode); break;
    case 0x40: this->rti(addressing_mode); break;
    case 0x48: this->pha(addressing_mode); break;
    case 0x50: this->bvc(addressing_mode); break;
    case 0x58: this->cli(addressing_mode); break;
    case 0x60: this->rts(addressing_mode); break;
    case 0x68: this->pla(addressing_mode); break;
    case 0x70: this->bvs(addressing_mode); break;
    case 0x78: this->sei(addressing_mode); break;
    case 0x88: this->dey(addressing_mode); break;
    case 0x8A: this->txa(addressing_mode); break;
    case 0x90: this->bcc(addressing_mode); break;
    case 0x98: this->tya(addressing_mode); break;
    case 0x9A: this->txs(addressing_mode); break;
    case 0xA8: this->tay(addressing_mode); break;
    case 0xAA: this->tax(addressing_mode); break;
    case 0xB0: this->bcs(addressing_mode); break;
    case 0xB8: this->clv(addressing_mode); break;
    case 0xBA: this->tsx(addressing_mode); break;
    case 0xC8: this->iny(addressing_mode); break;
    case 0xCA: this->dex(addressing_mode); break;
    case 0xD0: this->bne(addressing_mode); break;
    case 0xD8: this->cld(addressing_mode); break;
    case 0xE8: this->inx(addressing_mode); break;
    case 0xEA: this->nop(addressing_mode); break;
    case 0xF0: this->beq(addressing_mode); break;
    case 0xF8: this->sed(addressing_mode); break;

    case 0x01: case 0x05: case 0x09: case 0x0D: case 0x11:
    case 0x15: case 0x19: case 0x1D:
        this->ora(addressing_mode);
        break;

    case 0x06: case 0x0A: case 0x0E: case 0x16: case 0x1E:
        this->asl(addressing_mode);
        break;

    case 0x21: case 0x25: case 0x29: case 0x2D: case 0x31:
    case 0x35: case 0x39: case 0x3D:
        this->and_(addressing_mode);
        break;

    case 0x24: case 0x2C:
        this->bit(addressing_mode);
        break;

    case 0x26: case 0x2A: case 0x2E: case 0x36: case 0x3E:
        this->rol(addressing_mode);
        break;

    case 0x41: case 0x45: case 0x49: case 0x4D: case 0x51:
    case 0x55: case 0x59: case 0x5D:
        this->eor(addressing_mode);
        break;

    case 0x46: case 0x4A: case 0x4E: case 0x56: case 0x5E:
        this->lsr(addressing_mode);
        break;

    case 0x4C: case 0x6C:
        this->jmp(addressing_mode);
        break;

    case 0x61: case 0x65: case 0x69: case 0x6D: case 0x71:
    case 0x75: case 0x79: case 0x7D:
        this->adc(addressing_mode);
        break;

    case 0x66: case 0x6A: case 0x6E: case 0x76: case 0x7E:
        this->ror(addressing_mode);
        break;

    case 0x81: case 0x85: case 0x8D: case 0x91: case 0x95:
    case 0x99: case 0x9D:
        this->sta(addressing_mode);
        break;

    case 0x84: case 0x8C: case 0x94:
        this->sty(addressing_mode);
        break;

    case 0x86: case 0x8E: case 0x96:
        this->stx(addressing_mode);
        break;

    case 0xA0: case 0xA4: case 0xAC: case 0xB4: case 0xBC:
        this->ldy(addressing_mode);
        break;

    case 0xA1: case 0xA5: case 0xA9: case 0xAD: case 0xB1:
    case 0xB5: case 0xB9: case 0xBD:
        this->lda(addressing_mode);
        break;

    case 0xA2: case 0xA6: case 0xAE: case 0xB6: case 0xBE:
        this->ldx(addressing_mode);
        break;

    case 0xC0: case 0xC4: case 0xCC:
        this->cpy(addressing_mode);
        break;

    case 0xC1: case 0xC5: case 0xC9: case 0xCD: case 0xD1:
    case 0xD5: case 0xD9: case 0xDD:
        this->cmp(addressing_mode);
        break;

    case 0xC6: case 0xCE: case 0xD6: case 0xDE:
        this->dec(addressing_mode);
        break;

    case 0xE0: case 0xE4: case 0xEC:
        this->cpx(addressing_mode);
        break;

    case 0xE1: case 0xE5: case 0xE9: case 0xED: case 0xF1:
    case 0xF5: case 0xF9: case 0xFD:
        this->sbc(addressing_mode);
        break;

    case 0xE6: case 0xEE: case 0xF6: case 0xFE:
        this->inc(addressing_mode);
        break;

    }

    this->cycles += get_cycle_count(opcode);
}

std::vector<std::string> Processor::disassemble(const int count) const
{
    std::vector<std::string> instructions;

    auto instruction_pointer = this->program_counter;

    while (static_cast<int>(instructions.size()) < count)
    {
        const auto opcode = this->bus_->read_byte(instruction_pointer);

        const auto addressing_mode = get_addressing_mode(opcode);
        const auto addressing_mode_str = get_addressing_mode_str(addressing_mode);
        const auto mnemonic = get_opcode_mnemonic(opcode);
        const auto bytes_needed = get_bytes_needed(addressing_mode);

        const auto byte_1 = this->bus_->read_byte(instruction_pointer + 1);
        const auto byte_2 = this->bus_->read_byte(instruction_pointer + 2);

        char buffer[64];

        switch (bytes_needed)
        {
        case 0: sprintf_s(buffer, "$%04X    %02X             %s             {%s}", instruction_pointer, opcode, mnemonic.data(), addressing_mode_str.data()); break;
        case 1: sprintf_s(buffer, "$%04X    %02X %02X          %s %02X          {%s}", instruction_pointer, opcode, byte_1, mnemonic.data(), byte_1, addressing_mode_str.data()); break;
        case 2: sprintf_s(buffer, "$%04X    %02X %02X %02X       %s %02X %02X       {%s}", instruction_pointer, opcode, byte_1, byte_2, mnemonic.data(), byte_1, byte_2, addressing_mode_str.data()); break;

        }

        instruction_pointer += bytes_needed + 1;
        instructions.emplace_back(buffer);
    }

    return instructions;
}
