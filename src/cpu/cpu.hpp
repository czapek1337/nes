#pragma once

#include <memory>

#include "../bus/bus.hpp"

#undef OVERFLOW

enum class AddressingMode
{
    ABSOLUTE,
    ABSOLUTE_X,
    ABSOLUTE_Y,
    IMMEDIATE,
    IMPLIED,
    RELATIVE,
    INDIRECT,
    INDEXED_INDIRECT,
    INDIRECT_INDEXED,
    ZERO_PAGE,
    ZERO_PAGE_X,
    ZERO_PAGE_Y,
};

enum class ProcessorFlag
{
    CARRY,
    ZERO,
    INTERRUPT_DISABLE,
    DECIMAL_MODE,
    BREAK,
    UNUSED,
    OVERFLOW,
    NEGATIVE,
};

struct ProcessorFlags
{
    bool value[8];

    uint8_t get_uint() const;
    void set_uint(uint8_t uint);

};

class Processor
{
    std::shared_ptr<DataBus> bus_;

public:
    uint8_t accumulator;
    uint8_t index_x;
    uint8_t index_y;
    uint8_t stack_pointer;
    ProcessorFlags flags;
    uint16_t program_counter;
    uint64_t cycles;

    static int get_bytes_needed(AddressingMode addressing_mode);
    static int get_cycle_count(uint8_t opcode);
    static AddressingMode get_addressing_mode(uint8_t opcode);
    static std::string get_opcode_mnemonic(uint8_t opcode);
    static std::string make_flagsstring(uint8_t flags);
    static std::string get_addressing_mode_str(AddressingMode addressing_mode);

private:
    uint16_t resolve_addressing_mode(AddressingMode addressing_mode);

    bool get_flag(ProcessorFlag flag);
    void set_flag(ProcessorFlag flag, bool value);

    void push_byte(uint8_t value);
    void push_word(uint16_t value);

    uint8_t pop_byte();
    uint16_t pop_word();

public:
    explicit Processor(const std::shared_ptr<DataBus>& bus, uint16_t program_counter = 0);

    void power_up();
    void reset();

    void trace() const;
    void tick();

    std::vector<std::string> disassemble(int count) const;

protected:
    void xxx(AddressingMode addressing_mode);

    void adc(AddressingMode addressing_mode);
    void and_(AddressingMode addressing_mode);
    void asl(AddressingMode addressing_mode);
    void bcc(AddressingMode addressing_mode);
    void bcs(AddressingMode addressing_mode);
    void beq(AddressingMode addressing_mode);
    void bit(AddressingMode addressing_mode);
    void bmi(AddressingMode addressing_mode);
    void bne(AddressingMode addressing_mode);
    void bpl(AddressingMode addressing_mode);
    void brk(AddressingMode addressing_mode);
    void bvc(AddressingMode addressing_mode);
    void bvs(AddressingMode addressing_mode);
    void clc(AddressingMode addressing_mode);
    void cld(AddressingMode addressing_mode);
    void cli(AddressingMode addressing_mode);
    void clv(AddressingMode addressing_mode);
    void cmp(AddressingMode addressing_mode);
    void cpx(AddressingMode addressing_mode);
    void cpy(AddressingMode addressing_mode);
    void dec(AddressingMode addressing_mode);
    void dex(AddressingMode addressing_mode);
    void dey(AddressingMode addressing_mode);
    void eor(AddressingMode addressing_mode);
    void inc(AddressingMode addressing_mode);
    void inx(AddressingMode addressing_mode);
    void iny(AddressingMode addressing_mode);
    void jmp(AddressingMode addressing_mode);
    void jsr(AddressingMode addressing_mode);
    void lda(AddressingMode addressing_mode);
    void ldx(AddressingMode addressing_mode);
    void ldy(AddressingMode addressing_mode);
    void lsr(AddressingMode addressing_mode);
    void nop(AddressingMode addressing_mode);
    void ora(AddressingMode addressing_mode);
    void pha(AddressingMode addressing_mode);
    void php(AddressingMode addressing_mode);
    void pla(AddressingMode addressing_mode);
    void plp(AddressingMode addressing_mode);
    void rol(AddressingMode addressing_mode);
    void ror(AddressingMode addressing_mode);
    void rti(AddressingMode addressing_mode);
    void rts(AddressingMode addressing_mode);
    void sbc(AddressingMode addressing_mode);
    void sec(AddressingMode addressing_mode);
    void sed(AddressingMode addressing_mode);
    void sei(AddressingMode addressing_mode);
    void sta(AddressingMode addressing_mode);
    void stx(AddressingMode addressing_mode);
    void sty(AddressingMode addressing_mode);
    void tax(AddressingMode addressing_mode);
    void tay(AddressingMode addressing_mode);
    void tsx(AddressingMode addressing_mode);
    void txa(AddressingMode addressing_mode);
    void txs(AddressingMode addressing_mode);
    void tya(AddressingMode addressing_mode);

};
