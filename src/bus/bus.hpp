#pragma once

#include <array>
#include <cstdint>
#include <memory>
#include <vector>

// ReSharper disable once CppPolymorphicClassWithNonVirtualPublicDestructor
class BusDevice
{
public:
    virtual bool can_handle(uint16_t address, bool write = false) = 0;

    virtual uint8_t read_byte(uint16_t address) = 0;
    virtual uint16_t read_word(uint16_t address) = 0;

    virtual void write_byte(uint16_t address, uint8_t value) = 0;
    virtual void write_word(uint16_t address, uint16_t value) = 0;

};

class DataBus
{
    std::array<uint8_t, 2048> memory_;
    std::vector<std::shared_ptr<BusDevice>> devices_;

public:
    explicit DataBus(const std::vector<std::shared_ptr<BusDevice>>& devices = {});

    uint8_t read_byte(uint16_t address);
    uint16_t read_word(uint16_t address);

    void write_byte(uint16_t address, uint8_t value);
    void write_word(uint16_t address, uint16_t value);

};
