#include "bus.hpp"

DataBus::DataBus(const std::vector<std::shared_ptr<BusDevice>>& devices)
    : memory_({ 0 })
    , devices_(devices)
{
    // Initialized using initializer list
}

uint8_t DataBus::read_byte(const uint16_t address)
{
    for (const auto& device : this->devices_)
    {
        if (!device->can_handle(address, false))
            continue;

        return device->read_byte(address);
    }

    return this->memory_[address & 0x7FF];
}

uint16_t DataBus::read_word(const uint16_t address)
{
    for (const auto& device : this->devices_)
    {
        if (!device->can_handle(address, false))
            continue;

        return device->read_word(address);
    }

    return static_cast<uint16_t>(this->memory_[address & 0x7FF]) | this->memory_[address + 1 & 0x7FF] << 8;
}

void DataBus::write_byte(const uint16_t address, const uint8_t value)
{
    for (const auto& device : this->devices_)
    {
        if (!device->can_handle(address, true))
            continue;

        return device->write_byte(address, value);
    }

    this->memory_[address & 0x7FF] = value;
}

void DataBus::write_word(const uint16_t address, const uint16_t value)
{
    for (const auto& device : this->devices_)
    {
        if (!device->can_handle(address, true))
            continue;

        return device->write_word(address, value);
    }

    this->memory_[address & 0x7FF] = value & 0xFF;
    this->memory_[address + 1 & 0x7FF] = (value >> 8) & 0xFF;
}
