#pragma once

#include "../bus/bus.hpp"

// ReSharper disable once CppPolymorphicClassWithNonVirtualPublicDestructor
class AudioProcessor : public BusDevice
{
public:
    bool can_handle(uint16_t address, bool write = false) override;

    uint8_t read_byte(uint16_t address) override;
    uint16_t read_word(uint16_t address) override;

    void write_byte(uint16_t address, uint8_t value) override;
    void write_word(uint16_t address, uint16_t value) override;

};
