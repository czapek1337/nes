#include "apu.hpp"

bool AudioProcessor::can_handle(uint16_t, bool)
{
    return false;
}

uint8_t AudioProcessor::read_byte(uint16_t)
{
    return 0;
}

uint16_t AudioProcessor::read_word(uint16_t)
{
    return 0;
}

void AudioProcessor::write_byte(uint16_t, uint8_t)
{

}

void AudioProcessor::write_word(uint16_t, uint16_t)
{

}
