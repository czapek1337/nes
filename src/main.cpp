#define OLC_PGE_APPLICATION

#include <memory>

#include "apu/apu.hpp"
#include "bus/bus.hpp"
#include "cartridge/cartridge.hpp"
#include "cpu/cpu.hpp"
#include "ppu/ppu.hpp"

#include "olc_pge.hpp"

// ReSharper disable once CppPolymorphicClassWithNonVirtualPublicDestructor
class NesEmulator : public olc::PixelGameEngine
{
    std::shared_ptr<AudioProcessor> apu_;
    std::shared_ptr<Cartridge> cartridge_;
    std::shared_ptr<PictureProcessor> ppu_;
    std::shared_ptr<DataBus> data_bus_;
    std::shared_ptr<Processor> cpu_;

public:
    explicit NesEmulator(const std::string& rom_path)
    {
        this->apu_ = std::make_shared<AudioProcessor>();
        this->cartridge_ = std::make_shared<Cartridge>(rom_path);
        this->ppu_ = std::make_shared<PictureProcessor>();

        this->data_bus_ = std::make_shared<DataBus>(std::vector<std::shared_ptr<BusDevice>>({ this->apu_, this->cartridge_, this->ppu_ }));
        this->cpu_ = std::make_shared<Processor>(this->data_bus_, 0);

        this->sAppName = "NES emulator";
    }

    bool OnUserCreate() override
    {
        return true;
    }

    bool OnUserUpdate(const float elapsed_time) override
    {
        Clear(olc::DARK_BLUE);

        this->DrawSprite(16, 16, &this->ppu_->get_screen(), 2);
        this->DrawSprite(16, 512, &this->ppu_->get_name_table(0));
        this->DrawSprite(288, 512, &this->ppu_->get_name_table(1));

        const auto disassembly = this->cpu_->disassemble(32);
        auto instruction_index = 0;

        char buffer[256];

        sprintf_s(buffer, "A = $%02X      X = $%02X   Y = $%02X        SP = $%02X\nPC = $%04X   FLAGS = %s ($%02X)   CYCLES = %lld",
            this->cpu_->accumulator, this->cpu_->index_x, this->cpu_->index_y, this->cpu_->stack_pointer,
            this->cpu_->program_counter, Processor::make_flagsstring(this->cpu_->flags.get_uint()).data(),
            this->cpu_->flags.get_uint(), this->cpu_->cycles);

        this->DrawString(544, 16, buffer, olc::WHITE);

        for (const auto& instruction : disassembly)
        {
            this->DrawString(544, 48 + instruction_index++ * 10, instruction, instruction_index == 0 ? olc::CYAN : olc::WHITE);
        }

        auto frame_count = 0;

        while (!this->ppu_->frame_complete)
        {
            this->ppu_->tick();

            if (frame_count++ % 3 == 0)
                this->cpu_->tick();
        }

        this->ppu_->frame_complete = false;

        return true;
    }

};

int main()
{
    const auto emulator = std::make_shared<NesEmulator>("E:\\Resources\\NES\\ROMs\\nestest.nes");

    if (emulator->Construct(1024, 768, 1, 1, false))
        emulator->Start();
}
